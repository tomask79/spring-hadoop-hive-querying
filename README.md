Another example of combination of:

**Hadoop (2.6.0) + Hive (1.0.0) + Spring Framework**

This time I decided to test querying the HDFS data over Apache Hive and I have to say: **"You gotta love HIVE!"**

Why? Because I will never be coding map-reduce tasks manually again, Hive do it for you as it's needed.

Demo shows **how easy** is to get all rows with values bigger then average with Hive. Let's do it step by step:

1) We will create external HIVE table pointing to HDFS FS.


```
CREATE EXTERNAL TABLE weather_data (
city STRING, 
temperature INT,
year INT)
COMMENT 'This is table with weather data.'
ROW FORMAT DELIMITED FIELDS TERMINATED BY ","
LINES TERMINATED BY '\n' 
STORED AS TEXTFILE
LOCATION '/user/external-data.txt';
```

2) Now we will load the data into it:
```
LOAD DATA INPATH '/user/secondarysort/weather-data.txt' OVERWRITE INTO TABLE weather_data;
```

3) Now we want to print all rows from weather data table, with temperatures bigger then AVERAGE temperature from whole set. HiveQL is the following:

```
select 
A.city,
A.temperature,
A.year 
from weather_data A, 
(select avg(temperature) avg_temp from weather_data) wd 
WHERE A.temperature >= wd.avg_temp;
```

I attached all necessary scripts, see file applicationScripts.hiveQL.

To start the demo:

```
1) Launch hive 2 server
2) Simply launch class SpringHiveQueryDemo
```

Important files here are: 
```
applicationScripts.hiveQL
SpringHiveClientConnector
```

enjoy, T.





 
 