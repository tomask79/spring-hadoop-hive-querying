package myhadoop.hive.query.connector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import myhadoop.hive.query.dto.WeatherDataRow;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 * Spring Hive connector for querying the weather data set. 
 * 
 * @author tomask79
 *
 */
public class SpringHiveClientConnector {
	private JdbcTemplate jdbcTemplate;
	
	public void setJdbcTemplate(final JdbcTemplate template) { 
		this.jdbcTemplate = template; 
	}
	
	public List<WeatherDataRow> getWeatherDataBiggerThenAvg() {
		return jdbcTemplate.query(
						" select A.city, A.temperature, A.year "+ 
						" from weather_data A, "+
						" (select avg(temperature) avg_temp from weather_data) wd "+ 
						" WHERE A.temperature >= wd.avg_temp ",
			new RowMapper<WeatherDataRow>() {
				public WeatherDataRow mapRow(ResultSet arg0, int arg1)
									throws SQLException {
					return new WeatherDataRow(
						   arg0.getString(1),
						   arg0.getInt(2),
						   arg0.getInt(3));
				}		
		});
	}
}
