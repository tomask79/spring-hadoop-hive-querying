package myhadoop.hive.query.dto;

public class WeatherDataRow {
	private String city;
	private int temperature;
	private int year;
	
	public WeatherDataRow(String city, int temperature, int year) {
		this.city = city;
		this.temperature = temperature;
		this.year = year;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
}
