package myhadoop.hive.query.query_hive;

import myhadoop.hive.query.connector.SpringHiveClientConnector;
import myhadoop.hive.query.dto.WeatherDataRow;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * This demo shows all records from weather data 
 * with temperature bigger then AVG.
 *
 */
public class SpringHiveQueryDemo 
{
    public static void main( String[] args )
    {
    	ApplicationContext ctx = new 
        		ClassPathXmlApplicationContext("applicationContext.xml");
        
        SpringHiveClientConnector clientConnector = (SpringHiveClientConnector) 
        		ctx.getBean("hiveClientConnector");
        
        System.out.println("**** Data with temperatures bigger then AVERAGE **** ");
        for (WeatherDataRow row : clientConnector.getWeatherDataBiggerThenAvg()) {
        	System.out.println(row.getCity()+ " "+row.getTemperature());
        }
    }
}
